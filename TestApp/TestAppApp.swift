//
//  TestAppApp.swift
//  TestApp
//
//  Created by Vladyslav Poznyak on 16.02.2021.
//

import SwiftUI

@main
struct TestAppApp: App {
    var body: some Scene {
        WindowGroup {
            RegisterView(viewModel: RegisterViewModel())
        }
    }
}
