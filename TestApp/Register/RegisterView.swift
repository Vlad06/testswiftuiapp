//
//  RegisterView.swift
//  TestApp
//
//  Created by Vladyslav Poznyak on 16.02.2021.
//

import SwiftUI

struct RegisterView: View {
    @ObservedObject var viewModel: RegisterViewModel
    var body: some View {
        NavigationView {
            Form {
                Image(systemName: "cloud")
                    .resizable()
                    .foregroundColor(.blue)
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: .infinity, maxHeight: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    
                Section {
                    TextField("Username", text: $viewModel.username)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                
                Section {
                    VStack {
                        SecureField("Password", text: $viewModel.password)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                        SecureField("Confirm password", text: $viewModel.confirmPassword)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }
                }
                
                Button("Confirm") {
                    viewModel.createAccount()
                }
                .disabled(!viewModel.isInputValid)
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity)
                .foregroundColor(.white)
                .padding(.vertical, 8)
                .background(viewModel.isInputValid ? Color.green : Color.gray)
                .cornerRadius(10)
                .shadow(color: Color.gray, radius: 2, x: -2, y: 2)
                .padding(.horizontal, 0)
            }
            .listStyle(GroupedListStyle())
            .navigationTitle("Create an Account")
        }
        .alert(isPresented: $viewModel.accountCreated, content: {
            Alert(title: Text("You are logged in successfully"))
        })
        .onAppear() {
            UITableView.appearance().backgroundColor = .clear
            UITableViewCell.appearance().backgroundColor = .clear
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(viewModel: RegisterViewModel())
    }
}
