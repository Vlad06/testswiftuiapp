//
//  RegisterViewModel.swift
//  TestApp
//
//  Created by Vladyslav Poznyak on 16.02.2021.
//

import Foundation
import Combine

final class RegisterViewModel: ObservableObject, Identifiable {
    @Published var username = ""
    @Published var password = ""
    @Published var confirmPassword = ""
    @Published var isInputValid: Bool
    @Published var accountCreated = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(username: String = "", password: String = "") {
        self.username = username
        self.password = password
        self.confirmPassword = password
        self.isInputValid = Self.validate(username, password: password, confirmPassword: password)
        
        $username
            .combineLatest($password, $confirmPassword)
            .sink { (username, password, confirmPassword) in
                self.isInputValid = Self.validate(username, password: password, confirmPassword: confirmPassword)
            }
            .store(in: &subscriptions)
    
    }
    
    func createAccount() {
        guard isInputValid else {
            return
        }
        accountCreated = true
    }
    
    static func validate(_ username: String, password: String, confirmPassword: String) -> Bool {
        username.isEmpty == false &&
            password.count <= 8 &&
            password.rangeOfCharacter(from: .decimalDigits) != nil && //contains digits
            password == confirmPassword
    }
}
